import urllib.parse
import pycurl
import requests
import json

import selenium.common.exceptions
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.proxy import Proxy, ProxyType

import time
import sys

#Initialize
#f = open('proxies.txt', 'r')
#val = f.read()
#f.close()
#val = int(val)

#PROXY = ["45.82.245.34:3128", "208.80.28.208:8080"]
#if val > 0:
        #val -= 1
        #webdriver.DesiredCapabilities.FIREFOX['proxy'] = {
            #"httpProxy": PROXY[val],
            #"ftpProxy": PROXY[val],
            #"sslProxy": PROXY[val],
            #"proxyType": "MANUAL",
        #}
        #val += 1

#val = (val + 1) % 3
#f = open('proxies.txt', 'w')
#f.write(str(val))
#f.close()

driver = webdriver.Firefox()

#searchers: which stores to search (Aldi, eBay, Fresh Thyme, Kroger, Meijer, Target, Walmart, Whole Foods)
#query: item to search for
#zipcode: approximate location of stores to search in
#file: name of file to output to (use string unless you don't want to output to a file, in which case use 0)
#maximum: number of items to search for
#erase: erase the text in the file given (leave blank or 0 to do nothing)
def allScrape(searchers, query, zipcode, file=0, maximum=10, erase=0):
        collection = {"products": []}
        
        if file != 0 and erase == 1:
                f = open(file, 'w')
                f.write('')
                f.close()        
        
        if searchers[0] == 1:
                collection = aldiScrape(collection, query, maximum)
        
        if searchers[1] == 1:
                collection = eBaySearch(collection, query, maximum)
        
        if searchers[2] == 1:
                collection = freshThymeScrape(collection, query, maximum)
        
        if searchers[3] == 1:
                collection = krogerSearch(collection, query, zipcode, maximum)
        
        if searchers[4] == 1:
                collection = meijerScrape(collection, query, maximum)
        
        if searchers[5] == 1:
                collection = targetScrape(collection, query, zipcode, maximum)
        
        if searchers[6] == 1:
                collection = walmartScrape(collection, query, zipcode, maximum)
        
        if searchers[7] == 1:
                collection = wholeFoodsScrape(collection, query, zipcode, maximum)
        
        if file != 0:
                f = open(file, 'w')
                f.write(f'{collection}\n')
                f.close()
        
        return collection

def targetScrape(col, query, zipcode, maximum=30):
        driver.get(f'https://www.target.com/s?searchTerm={query}')
        #driver.implicitly_wait(5)
        
        try:
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='storeId-utilityNavBtn']")))
                closest_location.click()
                
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='zipOrCityState']")))
                closest_location.send_keys(zipcode)
                closest_location = driver.find_element_by_xpath("/html/body/div[8]/div/div/div/div/div/div/div/div[1]/div/div[3]/div[2]/button")
                closest_location.click()
                
                closest_location = driver.find_element_by_xpath("/html/body/div[8]/div/div/div/div/div/div/div/div[3]/div[2]/div[1]/button")
                closest_location.click()
                
                driver.get(f'https://www.target.com/s?searchTerm={query}')
        
        except:
                print("Location not selected correctly")       
        
        #Target specific scraping:
        try:
                #Grabs the first row of items (3 items per row)
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//ul/li[1]/div/div[2]/div/div/div/div[1]/div[1]/a")))
                names = driver.find_elements_by_xpath("//ul/li/div/div[2]/div/div/div/div[1]/div[1]/a")
                prices = driver.find_elements_by_xpath("//ul/li/div/div[2]/div/div/div/div[2]/span")
                for i in range(0, min(maximum, len(names))):
                        col["products"].append({"Source": {"Store": "Target", "Number": i + 1, "Separate Amount": False}, "Name": names[i].text, "Amount": "", "Price": prices[i].text})
                        
                #Scrolls the window to the bottom to load the rest of the results    
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                
                #Grabs the remaining items
                names = driver.find_elements_by_xpath("//ul/li/div/div/div[2]/div/div/div/div[1]/div[1]/a")
                prices = driver.find_elements_by_xpath("//ul/li/div/div/div[2]/div/div/div/div[2]/span")
                for i in range(0, min(maximum-3, len(names))):
                        col["products"].append({"Source": {"Store": "Target", "Number": i + 4, "Separate Amount": False}, "Name": names[i].text, "Amount": "", "Price": prices[i].text})
                
                return col
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

def walmartScrape(col, query, zipcode, maximum=44):
        driver.get(f'https://www.walmart.com/search/?query={query}')
        #driver.implicitly_wait(5)
        
        try:
                try:
                        closest_location = WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='header-Header-sparkButton']")))
                except:
                        driver.get(f'https://www.walmart.com/search/?query={query}')
                        closest_location = WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='header-Header-sparkButton']")))
                
                closest_location.click()
                
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='vh-location-button']")))  
                closest_location.click()
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='zipcode-location-form-input']")))
                closest_location.clear()
                closest_location.send_keys(zipcode)
                closest_location = driver.find_element_by_xpath("/html/body/div[1]/div[1]/div/div[1]/section/div[2]/div/div[3]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div[4]/div/div[2]/form/div[2]/button")
                closest_location.click()
                
                WebDriverWait(driver, 10).until(EC.staleness_of(closest_location))
                
                try:
                        closest_location = WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='header-Header-sparkButton']")))
                except:
                        driver.get(f'https://www.walmart.com/search/?query={query}')
                        closest_location = WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='header-Header-sparkButton']")))                
        
        except:
                print("Location not selected correctly")
        
        #Walmart specific scraping:
        try:
                #Grabs the items (usually around 40)
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//ul/li[1]/div/div[2]/div[5]/div/a/span")))
                try:
                        WebDriverWait(driver, 2).until(EC.presence_of_element_located((By.XPATH, "/html/body/div[13]/div/div/div[2]/div")))
                        driver.get(f'https://www.walmart.com/search/?query={query}')
                        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//ul/li[1]/div/div[2]/div[5]/div/a/span")))
                        
                except:
                        names = driver.find_elements_by_xpath("//ul/li/div/div[2]/div[5]/div/a/span")
                        fields = driver.find_elements_by_xpath("//ul/li/div/div[2]/div[7]/div/span/span")
                        for i in range(0, min(maximum, len(names))):
                                price = fields[i].text.split('\n')
                                col["products"].append({"Source": {"Store": "Walmart", "Number": i + 1, "Separate Amount": False}, "Name": names[i].text, "Amount": "", "Price": price[1]})
                        
                        return col
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

def wholeFoodsScrape(col, query, zipcode, maximum=20, sort='relevance', store='10404'):
        driver.get(f'https://products.wholefoodsmarket.com/search?sort={sort}&store={store}&text={query}')
        #driver.implicitly_wait(5)
        
        try:
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/div/div[1]/div/div[2]")))
                closest_location.click()
                
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/input")))
                closest_location.send_keys(zipcode)
                
                WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div[2]")))
                closest_location = driver.find_element_by_xpath("/html/body/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div[1]")
                closest_location.click()
        
        except:
                print("Location not selected correctly")
        
        #Whole Foods specific scraping:
        try:
                #Grabs the items (usually 20)
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//div/a/div[4]")))
                names = driver.find_elements_by_xpath("//div/a/div[4]")
                prices = driver.find_elements_by_xpath("//div/a/div[5]/div")
                for i in range(0, min(maximum, len(names))):
                        col["products"].append({"Source": {"Store": "Whole Foods", "Number": i + 1, "Separate Amount": False}, "Name": names[i].text, "Amount": "", "Price": prices[i].text})
                
                return col
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

#Not working
def costcoScrape():
        driver.get(f'https://www.costco.com/CatalogSearch?keyword=milk')
        driver.implicitly_wait(5)
        
        f = open('scrape.txt', 'w')
        f.write('')
        f.close()
        
        #Whole Foods specific scraping:
        try:
                file = open('scrape.txt', 'a')
                
                #/html/body/div[8]/div[2]/div[1]/ctl:cache/div/div[4]/div[1]/div[2]/div[2]/div[2]/div[2]/span/a
                #/html/body/div[8]/div[2]/div[1]/ctl:cache/div/div[4]/div[1]/div[2]/div[2]/div[2]/div[2]/span/a
                #Grabs the items (usually 24)
                names = driver.find_element_by_xpath("//span[@class='description']/a")
                file.write(f'Name: {names.text}')
                #prices = driver.find_elements_by_xpath("//div/a/div[5]/div")
                #for i in range(0, min(maximum, len(names))):
                        #file.write(f'Name: {names[i].text}, Price: {prices[i].text}\n')
                
                file.close()
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

#Sort of works; does not grab a random number of the first few results
def meijerScrape(col, query, maximum=40):
        driver.get(f'https://www.meijer.com/shop/en/search/?text={query}')
        driver.implicitly_wait(5)
        
        #try:
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='store-flyout-link-root']")))
                #closest_location.click()
                
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[7]/div/div[1]/div/div/section/div/div/div[1]/a")))
                #closest_location.click()
                #closest_location = driver.find_element_by_xpath("//*[@id='store-flyout-address']")
                #closest_location.click()
                #closest_location.send_keys(zipcode)
                
                #closest_location = driver.find_element_by_xpath("//*[@id='store-flyout-radius']")
                #closest_location.click()
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[7]/div/div[1]/div/div/section/div/div[1]/div[3]/div/select/option[3]")))
                #closest_location.click()
                
                #closest_location = driver.find_element_by_xpath("/html/body/div[7]/div/div[1]/div/div/section/div/div[1]/div[4]/button")
                #closest_location.click()
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[7]/div/div[1]/div/div/section/div/div[2]/div[5]/button[2]")))
                #closest_location.click()
        
        #except:
                #print("Location not selected correctly")
        
        #Meijer specific scraping:
        try:
                #Grabs the items (usually 48)
                #milk
                WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[1]/div/div[2]/div[2]/a")))
                names = driver.find_elements_by_xpath("//div/div/div[2]/div[2]/a")
                prices = driver.find_elements_by_xpath("//div/div/div[2]/div[2]/div[2]/div[1]/div[1]/span[2]")
                #notebook
                #names = driver.find_elements_by_xpath("/html/body/main/div[3]/div[5]/div[2]/div[6]/div[5]/div/div/div[2]/div[2]/a")
                
                i = 0
                while i < min(maximum, len(names)):
                        if names[i].text == '':
                                names.pop(i)
                                prices.pop(i)
                        else:
                                col["products"].append({"Source": {"Store": "Meijer", "Number": i + 1, "Separate Amount": False}, "Name": names[i].text, "Amount": "", "Price": "$" + prices[i].text})
                                i += 1
                
                return col
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

#Need to account for case that amount is not listed (too slow with FreshThyme method)
def aldiScrape(col, query, maximum=40):
        driver.get('https://www.instacart.com/store/aldi/search_v3/milk')
        #driver.implicitly_wait(5)
        try:
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/header/div/div/div[7]/div[2]/div/div/div[1]/div[3]/div/button")))
                #closest_location.click()
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/header/div/div/div[7]/div[2]/div/div/div[1]/div[3]/div/button/svg[1]/path")))
                #closest_location.clear()
                #closest_location.send_keys(zipcode)
                
                #closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/header/div/div[2]/div[2]/div/div/div[1]/div[4]/button[1]")))
                closest_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/header/div/div/div[7]/div[2]/div/div/div[1]/div[4]/button[1]")))
                closest_location.click()
        
        except:
                print("Location not selected correctly")
        
        driver.get(f'https://www.instacart.com/store/aldi/search_v3/{query}')
        #driver.implicitly_wait(5)
        
        #Aldi specific scraping:
        try:
                #Grabs the items (usually around 45)
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//ul/li[1]/div/a/div/div/div[3]/div/span[1]")))
                names = driver.find_elements_by_xpath("//ul/li/div/a/div/div/div[3]/div/span[1]")
                names2 = driver.find_elements_by_xpath("//ul/li/div/a/div/div/div[2]/div/span[1]")
                names.extend(names2)
                
                amounts = driver.find_elements_by_xpath("//ul/li/div/a/div/div/div[3]/div/span[2]")
                amounts2 = driver.find_elements_by_xpath("//ul/li/div/a/div/div/div[2]/div/span[2]")
                amounts.extend(amounts2)
                
                prices = driver.find_elements_by_xpath("//ul/li/div/a/div/div/div[3]/div/div[1]/div/div/span/span")
                prices2 = driver.find_elements_by_xpath("//ul/li/div/a/div/div/div[2]/div/div[1]/div/div/span/span")
                prices.extend(prices2)
                for i in range(0, min(maximum, len(names))):
                        col["products"].append({"Source": {"Store": "Aldi", "Number": i + 1, "Separate Amount": True}, "Name": names[i].text, "Amount": amounts[i].text, "Price": prices[i].text})
                
                return col
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

#Slow
def freshThymeScrape(col, query, maximum=20):
        driver.get(f'https://discover.freshthyme.com/search?search_term={query}')
        #driver.implicitly_wait(4)
        try:
                #exit_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='shopping-selector-search-cities']")))
                #exit_location.click()
                #closest_location.send_keys(zipcode)
                
                exit_location = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='shopping-selector-parent-process-modal-close-click']")))
                exit_location.click()
                
                #Select Lafyette (always?)
                #closest_location = driver.find_element_by_xpath('//*[@id="shopping-selector-update-home-store-121-instore"]')
                #closest_location.click()
        
        except:
                print("Button not selected correctly")
        
        #driver.implicitly_wait(4)
        
        #Fresh Thyme specific scraping:
        try:
                #Grabs the items (usually 60)
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//ol/li[1]/div/div[4]/button/div[1]")))
                names = driver.find_elements_by_xpath("//ol/li/div/div[4]/button/div[1]")
                prices = driver.find_elements_by_xpath("//ol/li/div/div[4]/div[2]/div/div/react-product-price/div/div[2]/span[1]/span[1]")
                for i in range(0, min(maximum, len(names))):
                        try:
                                amounts = driver.find_element_by_xpath(f"//ol/li[{i}]/div/div[4]/button/div[2]")
                                amount = ', ' + amounts.text
                        except:
                                amount = ''
                        col["products"].append({"Source": {"Store": "Fresh Thyme", "Number": i + 1, "Separate Amount": True}, "Name": names[i].text, "Amount": amount, "Price": prices[i].text})
                
                return col
                        
        except selenium.common.exceptions.NoSuchElementException:
                print("Element(s) not found")
        except:
                print("Unknown error:", sys.exc_info()[0])

def eBaySearch(col, keywords, number=100, local=0):
        #Parses the search into web address format
        query = urllib.parse.quote(keywords)
        pages = 1
        leftover = number
        
        if number > 100:
                pages = (number // 100) + 1
                leftover = number % 100
                number = 100
                
        #Empties ebayData.json if it exists; creates it otherwise
        f = open('ebayData.json', 'w')
        f.write('{"products": [')
        f.close()
    
        #Constructs the url
        for j in range(pages - 1):
                url = f'https://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=JoelBail-WallySho-PRD-f500f24f5-84c37ec0&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&paginationInput.entriesPerPage={number}&paginationInput.pageNumber={j + 1}&keywords={query}'
                if local != 0:
                        url += f'&buyerPostalCode={local}&sortOrder=Distance'
                
                #Print each search result with its properties, including a separate section for title, price, and location
                response = requests.get(url)
                output = json.loads(response.text)
                file = open('ebayData.json', 'a')
                i = 0
                for x in output['findItemsByKeywordsResponse'][0]['searchResult'][i]['item']:
                        try:
                                if i < number - 1:
                                        file.write(f'\n{x},')
                                else:
                                        file.write(f'\n{x}')
                                i += 1
                        except:
                                i += 1
                i = 0
                file.write('\n]}\nInfo:')
                for x in output['findItemsByKeywordsResponse'][0]['searchResult'][i]['item']:
                        try:
                                name = x["title"][0]
                                price = x["sellingStatus"][0]["convertedCurrentPrice"][0]["__value__"]
                                location = x["location"][0]
                                col["products"].append({"Source": {"Store": "eBay", "Number": i + (j * 100) + 1, "Separate Amount": False}, "Name": name, "Amount": "", "Price": "$" + price})
                                file.write(f'\nTitle: {name}')
                                file.write(f', Price: {price}')
                                file.write(f', Location: {location}')
                                i += 1
                        except:
                                col["products"].pop()
                                i += 1
                file.close()
        
        url = f'https://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=JoelBail-WallySho-PRD-f500f24f5-84c37ec0&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&paginationInput.entriesPerPage={leftover}&paginationInput.pageNumber={pages}&keywords={query}'
        if local != 0:
                url += f'&buyerPostalCode={local}&sortOrder=Distance'
        
        #Print each search result with its properties, including a separate section for title, price, and location
        response = requests.get(url)
        output = json.loads(response.text)
        file = open('ebayData.json', 'a')
        i = 0
        for x in output['findItemsByKeywordsResponse'][0]['searchResult'][i]['item']:
                try:
                        if i < number - 1:
                                file.write(f'\n{x},')
                        else:
                                file.write(f'\n{x}')
                        i += 1
                except:
                        i += 1
        i = 0
        file.write('\n]}\nInfo:')
        for x in output['findItemsByKeywordsResponse'][0]['searchResult'][i]['item']:
                try:
                        name = x["title"][0]
                        price = x["sellingStatus"][0]["convertedCurrentPrice"][0]["__value__"]
                        location = x["location"][0]
                        col["products"].append({"Source": {"Store": "eBay", "Number": i + ((pages - 1) * 100) + 1, "Separate Amount": False}, "Name": name, "Amount": "", "Price": "$" + price})
                        file.write(f'\nTitle: {name}')
                        file.write(f', Price: {price}')
                        file.write(f', Location: {location}')
                        i += 1
                except:
                        col["products"].pop()
                        i += 1
        file.close()        
        
        return col
                    
def krogerSearch(col, keywords, zipcode, number=50):
        if number > 50:
                number = 50
                print('Kroger does not support more than 50 searches at once')
        
        file = open('time.txt', 'r')
        if abs(float(file.read()) - time.time()) > 1790:
                file.close()
                file = open('time.txt', 'w')
                file.write(f'{time.time()}')
                file.close()
                
                with open('token.json', 'wb') as f:
                        crl = pycurl.Curl()
                        
                        crl.setopt(pycurl.URL, 'https://api-ce.kroger.com/v1/connect/oauth2/token')
                        crl.setopt(pycurl.HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded', 'Authorization: Basic d2FsbHlzaG9wLTU2N2M1YmRkMTI1YjBmNzI2ZWY2YjU3MzE4ODE0MDljNDQ3NzAxNTg3MzExMTc2ODAwMTpQamJYQ29SSGc0UDd6NE4yc0c1QkxISmN4Q0VoQzJMU1lmTG0xRWg4'])
                        crl.setopt(pycurl.POSTFIELDS, 'grant_type=client_credentials&scope=product.compact')
                        crl.setopt(pycurl.CUSTOMREQUEST, "POST")
                        
                        crl.setopt(pycurl.WRITEDATA, f)
                        
                        crl.perform()
                        crl.close()            
        else:
                file.close()
            
        f = open('token.json', 'r')
        f.read(35)
        token = f.read(905)
        f.close()
        
        with open('krogerData.json', 'wb') as f:
                crl = pycurl.Curl()
                
                crl.setopt(pycurl.URL, f'https://api-ce.kroger.com/v1/locations?filter.zipCode.near={zipcode}&filter.chain=Kroger&filter.limit=1')
                crl.setopt(pycurl.HTTPHEADER, [f'Authorization: Bearer {token}', 'Accept: application/json\\'])
                crl.setopt(pycurl.CUSTOMREQUEST, "GET")
                
                crl.setopt(pycurl.WRITEDATA, f)
                
                crl.perform()
        
        f = open('krogerData.json', 'r')
        raw = f.read()
        location = raw[24:32]
        f.close()
        
        with open('krogerData.json', 'wb') as f:
                crl.setopt(pycurl.URL, f'https://api-ce.kroger.com/v1/products?filter.term={keywords}&filter.limit={number}&filter.locationId={location}')
                crl.setopt(pycurl.HTTPHEADER, [f'Authorization: Bearer {token}', 'Accept: application/json\\'])
                crl.setopt(pycurl.CUSTOMREQUEST, "GET")
                
                crl.setopt(pycurl.WRITEDATA, f)
                
                crl.perform()
                crl.close()
                
        f = open('krogerData.json', 'r')
        raw = json.loads(f.read())
        f.close()
        
        for i in range(0, number):
                item = raw["data"][i]
                try:
                        col["products"].append({"Source": {"Store": "Kroger", "Number": i + 1, "Separate Amount": False}, "Name": item["description"], "Amount": "", "Price": "$" + str(item["items"][0]["price"]["regular"])})
                except:
                        col["products"].pop()
        
        return col

allScrape([0,0,0,1,0,0,0,0], "milk", "46214", "data.json", 51, 1)
#costcoScrape()

#Close browser
driver.close()

#Important things to update: Setting location, getting more results
#Implement Logan's parser to separate key item from units
#Account for store availability (like Walmart); implement going through individual items to get prices